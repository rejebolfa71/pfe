-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : jeu. 01 juin 2023 à 21:38
-- Version du serveur : 10.4.28-MariaDB
-- Version de PHP : 8.0.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `gpro`
--

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

CREATE TABLE `category` (
  `id` bigint(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `category`
--

INSERT INTO `category` (`id`, `description`, `name`, `photo`) VALUES
(1, NULL, 'Computers', NULL),
(2, NULL, 'Printers', NULL),
(3, NULL, 'Smart phones', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `date` datetime DEFAULT NULL,
  `total_amount` double NOT NULL,
  `client_id` bigint(20) DEFAULT NULL,
  `payment_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `order_item`
--

CREATE TABLE `order_item` (
  `id` bigint(20) NOT NULL,
  `price` double NOT NULL,
  `quantity` int(11) NOT NULL,
  `order_id` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `payment`
--

CREATE TABLE `payment` (
  `id` bigint(20) NOT NULL,
  `card_number` bigint(20) NOT NULL,
  `card_type` varchar(255) DEFAULT NULL,
  `date_payment` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `available` bit(1) NOT NULL,
  `current_price` double NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `photo_name` varchar(255) DEFAULT NULL,
  `promotion` bit(1) NOT NULL,
  `selected` bit(1) NOT NULL,
  `category_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `product`
--

INSERT INTO `product` (`id`, `available`, `current_price`, `description`, `name`, `photo_name`, `promotion`, `selected`, `category_id`) VALUES
(1, b'1', 6105, NULL, '9SGPa8CisISZFXBhsK', 'unknown.png', b'1', b'1', 1),
(2, b'1', 4631, NULL, 'wyyNEvPfmHvCzpPXC4', 'unknown.png', b'1', b'0', 1),
(3, b'1', 5668, NULL, 'qREV2IyDBFI3AmBMME', 'unknown.png', b'1', b'0', 1),
(4, b'1', 6589, NULL, 'h3vBaYpCWDMbyBJYu5', 'unknown.png', b'1', b'1', 1),
(5, b'0', 6006, NULL, 'dYs0TieulxTExHYE6v', 'unknown.png', b'0', b'0', 1),
(6, b'1', 6292, NULL, 'gLb2cpxDnA1M2FaNZP', 'unknown.png', b'1', b'1', 1),
(7, b'1', 6456, NULL, 'ueJwjduSy8VQEKLoo3', 'unknown.png', b'1', b'1', 1),
(8, b'0', 7400, NULL, 'bxNFiXreHvdgasjGJd', 'unknown.png', b'1', b'0', 1),
(9, b'0', 1035, NULL, 'jXaRgU1Ab4FSZq086P', 'unknown.png', b'0', b'1', 1),
(10, b'0', 7824, NULL, '6MD02HkSCP82IxVKby', 'unknown.png', b'0', b'1', 1),
(11, b'0', 2715, NULL, 'ci41BPDjYf0IT2noaS', 'unknown.png', b'1', b'1', 2),
(12, b'0', 7134, NULL, 'p5ssAf6Xl7kLQE1o0i', 'unknown.png', b'1', b'1', 2),
(13, b'0', 6113, NULL, 'bkmC4AWolpOnuzlW5n', 'unknown.png', b'0', b'0', 2),
(14, b'0', 3373, NULL, 'hX0FJR67vXybYwEAEa', 'unknown.png', b'0', b'1', 2),
(15, b'0', 954, NULL, 'dPULSt409He8XjBMuj', 'unknown.png', b'0', b'0', 2),
(16, b'1', 2275, NULL, 'lCBJHCeR3yaQr2HjAO', 'unknown.png', b'1', b'0', 2),
(17, b'0', 1075, NULL, '5OIjFu6xGdwsBV1c6I', 'unknown.png', b'0', b'1', 2),
(18, b'0', 542, NULL, 'eALB9GjpArs0x5vw4J', 'unknown.png', b'0', b'1', 2),
(19, b'1', 798, NULL, 'y9bGCNYLsWbFZFi7bg', 'unknown.png', b'1', b'0', 2),
(20, b'1', 5017, NULL, 'zZmpctbfFTbbSOiy2T', 'unknown.png', b'1', b'0', 2),
(21, b'0', 5243, NULL, 'U7MlC6ePZVPfk1ZtZC', 'unknown.png', b'0', b'1', 3),
(22, b'0', 821, NULL, 'aiIzmCVAmBOPqW3lc3', 'unknown.png', b'0', b'1', 3),
(23, b'1', 754, NULL, 'rmhRT5cEIikiE9UpTq', 'unknown.png', b'1', b'0', 3),
(24, b'0', 9554, NULL, 'oWcWawys7Ef3s3Wv2C', 'unknown.png', b'1', b'1', 3),
(25, b'1', 6469, NULL, '3t5hhNNSeRHFj6ekls', 'unknown.png', b'0', b'1', 3),
(26, b'1', 6181, NULL, '8PjaWg84LQ5g1eZ8Bu', 'unknown.png', b'1', b'0', 3),
(27, b'1', 1760, NULL, 'Los6dRkDzo1FfNjEuL', 'unknown.png', b'1', b'0', 3),
(28, b'0', 2113, NULL, 'CuM0JQ6ogoNVbP8WF7', 'unknown.png', b'1', b'0', 3),
(29, b'0', 5530, NULL, 'RFUMLGk6dq17AaJKzS', 'unknown.png', b'0', b'0', 3),
(30, b'1', 7457, NULL, 'mqXIKgy0HqJgb0KHZu', 'unknown.png', b'1', b'1', 3);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK17yo6gry2nuwg2erwhbaxqbs9` (`client_id`),
  ADD KEY `FKag8ppnkjvx255gj7lm3m18wkj` (`payment_id`);

--
-- Index pour la table `order_item`
--
ALTER TABLE `order_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKt4dc2r9nbvbujrljv3e23iibt` (`order_id`),
  ADD KEY `FK551losx9j75ss5d6bfsqvijna` (`product_id`);

--
-- Index pour la table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK1mtsbur82frn64de7balymq9s` (`category_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `order_item`
--
ALTER TABLE `order_item`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `FK17yo6gry2nuwg2erwhbaxqbs9` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  ADD CONSTRAINT `FKag8ppnkjvx255gj7lm3m18wkj` FOREIGN KEY (`payment_id`) REFERENCES `payment` (`id`);

--
-- Contraintes pour la table `order_item`
--
ALTER TABLE `order_item`
  ADD CONSTRAINT `FK551losx9j75ss5d6bfsqvijna` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `FKt4dc2r9nbvbujrljv3e23iibt` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`);

--
-- Contraintes pour la table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `FK1mtsbur82frn64de7balymq9s` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
