# Use the official MySQL 8.0 image as the base image
FROM mysql:8.0

# Set environment variables for MySQL configuration
ENV MYSQL_DATABASE=gpro
ENV MYSQL_USER=gprouser
ENV MYSQL_PASSWORD=1234
ENV MYSQL_ROOT_PASSWORD=1234
# Copy the database.sql file to the Docker image
COPY gpro.sql /docker-entrypoint-initdb.d/

